package com.pl.hospitalify

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.pl.hospitalify.view.StartingActivity
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import strikt.api.expect
import strikt.assertions.isEqualTo

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginTest {

    companion object {
        private val LOGIN = "SampleLogin"
        private val PASSWORD = "SamplePassword"
    }

    @get:Rule
    var activityScenarioRule = activityScenarioRule<StartingActivity>()

    val server = ServiceMock()

    @After
    fun cleanUp() {
        server.shutdown()
    }

    @Test
    fun credentialsFromLoginScreenUsedToAuthorize() {
        server.dispatchOnce(authorized())
        onView(withId(R.id.loginButton))
            .perform(click())
        onView(withId(R.id.userNameEdit))
            .perform(clearText())
            .perform(typeText(LOGIN), closeSoftKeyboard())
        onView(withId(R.id.passwordEdit))
            .perform(clearText())
            .perform(typeText(PASSWORD), closeSoftKeyboard())

        onView(withId(R.id.loginButton))
            .perform(click())

        expect {
            that(server.takeRequest().body.readUtf8()) {
                isEqualTo(login(LOGIN, PASSWORD))
            }
        }
    }
}