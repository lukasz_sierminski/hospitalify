package com.pl.hospitalify

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.pl.hospitalify.model.Profile
import com.pl.hospitalify.view.MainActivity
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ProfileTest {

    companion object {
        private val USERNAME = "username"
        private val FIRST_NAME = "first name"
        private val LAST_NAME = "last name"
        private val EMAIL = "email"
        private val PESEL = "12312312312"
        private val BIRTH_DATE = "11-01-1901"
        private val PHONE_NUMBER = "22 222 22 22"
    }

    val server = ServiceMock()

    @Before
    fun setup() {
        val profile = Profile(
            1, 1, USERNAME, FIRST_NAME, LAST_NAME, EMAIL, BIRTH_DATE, PHONE_NUMBER, PESEL
        )
//        server.dispatchOnce(authorized())
        server.dispatchOnce(profile(profile))
    }

    @After
    fun cleanUp() {
        server.shutdown()
    }

    @Test
    fun profileDataIsCorrectlyDisplayed() {
        launchActivity<MainActivity>()
        onView(withId(R.id.userName)).check(matches(withText(USERNAME)))
        onView(withId(R.id.firstName)).check(matches(withText(FIRST_NAME)))
        onView(withId(R.id.lastName)).check(matches(withText(LAST_NAME)))
        onView(withId(R.id.email)).check(matches(withText(EMAIL)))
        onView(withId(R.id.pesel)).check(matches(withText(PESEL)))
        onView(withId(R.id.birthDate)).check(matches(withText(BIRTH_DATE)))
        onView(withId(R.id.phone)).check(matches(withText(PHONE_NUMBER)))
    }
}