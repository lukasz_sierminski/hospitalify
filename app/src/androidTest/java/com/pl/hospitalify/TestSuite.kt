package com.pl.hospitalify

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    LoginTest::class,
    ProfileTest::class,
    ClinicTest::class
)
class TestSuite