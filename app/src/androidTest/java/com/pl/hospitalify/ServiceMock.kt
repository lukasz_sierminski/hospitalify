package com.pl.hospitalify

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import java.util.*

class ServiceMock {

    private var defaultResponse = MockResponse().setBody("ASD")

    private val server = MockWebServer()
    private val port = 45555
    private val responses = ArrayDeque<MockResponse>()

    private val dispatcher = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest?): MockResponse {
            Logger.debug("Dispatching request ${request?.requestLine}")
            val response = responses.poll() ?: defaultResponse
            Logger.debug("Dispatching Response : ${response.body}")
            return response
        }
    }

    init {
        server.setDispatcher(dispatcher)
        server.start(port)
    }

    fun takeRequest() = server.takeRequest()

    fun dispatchOnce(response: MockResponse) = responses.add(response)

    fun dispatch(mockResponses: List<MockResponse>) = responses.addAll(mockResponses)

    fun dispatch(response: MockResponse) {
        defaultResponse = response
    }

    fun recordedRequests(): List<RecordedRequest> {
        return IntRange(1, server.requestCount)
            .map { server.takeRequest() }
    }

    fun recordedRequestsSize(): Int = server.requestCount

    fun url(): String = server.url("/").toString()

    fun shutdown() {
        dispatcher.shutdown()
        server.shutdown()
    }
}