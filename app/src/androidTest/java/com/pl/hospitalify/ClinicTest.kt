package com.pl.hospitalify

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.pl.hospitalify.model.Clinic
import com.pl.hospitalify.model.Doctor
import com.pl.hospitalify.model.Profile
import com.pl.hospitalify.view.MainActivity
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import strikt.api.expect
import java.lang.Thread.sleep

@LargeTest
@RunWith(AndroidJUnit4::class)
class ClinicTest {

    companion object {
        private const val CLINIC_ID = 43
        private const val UNIT_NAME = "Test unit name 1"
        private const val ADDRESS = "address unit 1"
        private const val POSTAL_CODE = "unit code 1"
        private const val CITY = "Lodz"

        private const val DOCTOR_ID = 13
        private const val DOCTOR_FIRST_NAME = "John"
        private const val DOCTOR_LAST_NAME = "Smith"
        private const val DOCTOR_MAIL = "john.smith@healthcare.com"
        private const val DOCTOR_PHONE = "223433345"
        private const val DOCTOR_SPECIALISATION = "Surgeon"
    }

    val server = ServiceMock()

    @Before
    fun setup() {
        val testClinic = Clinic(
            CLINIC_ID,
            UNIT_NAME,
            ADDRESS,
            POSTAL_CODE,
            CITY
        )
        server.dispatchOnce(profile(Profile(0, 0, "", "", "", "", "10-10-2010", "", "")))
        server.dispatchOnce(clinics(testClinic))
    }

    @After
    fun cleanUp() {
        server.shutdown()
    }

    @Test
    fun clinicDataIsCorrectlyDisplayed() {
        launchActivity<MainActivity>()
        onView(withId(R.id.visits)).perform(click())

        onView(withId(R.id.name)).check(matches(withText(UNIT_NAME)))
        onView(withId(R.id.address)).check(matches(withText(ADDRESS)))
        onView(withId(R.id.postalCode)).check(matches(withText(POSTAL_CODE)))
        onView(withId(R.id.city)).check(matches(withText(CITY)))
    }

    @Test
    fun doctorDataIsCorrectlyDisplayed() {
        val doctor = Doctor(
            DOCTOR_ID,
            DOCTOR_FIRST_NAME,
            DOCTOR_LAST_NAME,
            DOCTOR_MAIL,
            DOCTOR_PHONE,
            DOCTOR_SPECIALISATION
        )
        server.dispatch(doctor(doctor))
        launchActivity<MainActivity>()
        onView(withId(R.id.visits)).perform(click())

        onView(withText(UNIT_NAME)).perform(click())

        onView(withId(R.id.specialisationSpinner)).perform(scrollTo(), click())
        onData(`is`(DOCTOR_SPECIALISATION)).perform(click())
        onView(withId(R.id.specialisationSpinner)).check(matches(withSpinnerText(DOCTOR_SPECIALISATION)))

        onView(withId(R.id.doctorsRecyclerView)).perform(scrollTo())
        onView(withId(R.id.firstName)).check(matches(withText(DOCTOR_FIRST_NAME)))
        onView(withId(R.id.lastName)).check(matches(withText(DOCTOR_LAST_NAME)))
        onView(withId(R.id.email)).check(matches(withText(DOCTOR_MAIL)))

        expect {
            that(server.takeRequest().path) {
                endsWith("unit=$CLINIC_ID")
            }
        }
    }
}