package com.pl.hospitalify

import com.pl.hospitalify.model.Clinic
import com.pl.hospitalify.model.Doctor
import com.pl.hospitalify.model.Profile
import okhttp3.mockwebserver.MockResponse

fun doctor(doctor: Doctor) = MockResponse().setBody("""[{ "id" : "${doctor.id}", "firstName" : "${doctor.firstName}",
  "lastName": "${doctor.lastName}",
  "email": "${doctor.email}",
  "phoneNumber": "${doctor.phoneNumber}",
  "specialisation": "${doctor.specialisation}"
}]"""

)

fun clinics(clinic: Clinic) = MockResponse().setBody("""[{ "id" : "${clinic.id}", "unitName" : "${clinic.unitName}",
  "address": "${clinic.address}",
  "postalCode": "${clinic.postalCode}",
  "city": "${clinic.city}"
}]""")

fun profile(profile: Profile) = MockResponse().setBody("""{ "id" : "${profile.id}", "patientId" : "${profile.patientId}",
  "username": "${profile.username}",
  "firstName": "${profile.firstName}",
  "lastName": "${profile.lastName}",
  "email": "${profile.email}",
  "birthDate": "${profile.birthDate}",
  "phoneNumber": "${profile.phoneNumber}",
  "pesel": "${profile.pesel}"
}""")

fun authorized(): MockResponse {
    return MockResponse().setBody("""{ "token" : "test-token" }""")
}