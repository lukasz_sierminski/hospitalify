package com.pl.hospitalify.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pl.hospitalify.Logger
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.Doctor

class DoctorViewModel : ViewModel() {

    private lateinit var doctors: MutableLiveData<List<Doctor>>

    fun getDoctors(unitId: Int) : MutableLiveData<List<Doctor>> {
        if (!::doctors.isInitialized) {
            doctors = MutableLiveData()
            loadDoctors(unitId)
        }
        return doctors
    }

    private fun loadDoctors(unitId: Int) {
        ServiceProvider.healthcareRepository.getDoctors(unitId, onSuccess = {
            Logger.debug("Retrieved doctors data successfully, code: ${it.code()}")
            if (it.isSuccessful) {
                doctors.value = it.body()
            }
        }, onFailure = {
            Logger.debug("Failed to retrieve doctors data, ${it.message}")
        })
    }
}