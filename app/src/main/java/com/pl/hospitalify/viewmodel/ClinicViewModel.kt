package com.pl.hospitalify.viewmodel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pl.hospitalify.Logger
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.Clinic

class ClinicViewModel : ViewModel() {

    private lateinit var clinics: MutableLiveData<List<Clinic>>

    fun getClinics() : MutableLiveData<List<Clinic>> {
        if (!::clinics.isInitialized) {
            clinics = MutableLiveData()
            loadClinics()
        }
        return clinics
    }

    private fun loadClinics() {
        ServiceProvider.healthcareRepository.getHealthCareUnits(onSuccess = {
            Logger.debug("Retrieved units data successfully, code: ${it.code()}")
            if (it.isSuccessful) {
                clinics.value = it.body()
            }
        }, onFailure = {
            Logger.debug("Failed to retrieve units data, ${it.message}")
        })
    }
}