package com.pl.hospitalify.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pl.hospitalify.Logger
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.ExistingVisit

class ExistingVisitViewModel : ViewModel() {

    private lateinit var visits: MutableLiveData<List<ExistingVisit>>

    fun getVisits() : MutableLiveData<List<ExistingVisit>> {
        if (!::visits.isInitialized) {
            visits = MutableLiveData()
            loadVisits()
        }
        return visits
    }

    private fun loadVisits() {
        ServiceProvider.healthcareRepository.getVisitsHistory(
            onSuccess = {
            Logger.debug("Retrieved visits data successfully, code: ${it.code()}")
            if (it.isSuccessful) {
                visits.value = it.body()
            }
        }, onFailure = {
            Logger.debug("Failed to retrieve visits data, ${it.message}")
        })
    }
}