package com.pl.hospitalify.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pl.hospitalify.Logger
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.Prescription

class PrescriptionViewModel : ViewModel() {

    private lateinit var prescriptions: MutableLiveData<List<Prescription>>

    fun getPrescriptions() : MutableLiveData<List<Prescription>> {
        if (!::prescriptions.isInitialized) {
            prescriptions = MutableLiveData()
            loadPrescriptions()
        }
        return prescriptions
    }

    private fun loadPrescriptions() {
        ServiceProvider.healthcareRepository.getPrescriptions(
            onSuccess = {
            Logger.debug("Retrieved units data successfully, code: ${it.code()}")
            if (it.isSuccessful) {
                prescriptions.value = it.body()
            }
        }, onFailure = {
            Logger.debug("Failed to retrieve units data, ${it.message}")
        })
    }
}