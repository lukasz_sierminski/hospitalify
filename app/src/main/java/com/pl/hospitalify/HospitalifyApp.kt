package com.pl.hospitalify

import android.app.Application
import com.pl.hospitalify.data.ServiceProvider

class HospitalifyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        ServiceProvider.initialize(applicationContext)
    }
}