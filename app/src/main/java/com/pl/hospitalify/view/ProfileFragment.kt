package com.pl.hospitalify.view

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.pl.hospitalify.R
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.Profile
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var progressContainer: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ServiceProvider.healthcareRepository.getProfile(onSuccess = { response ->
            if (response.isSuccessful) {
                response.body()?.let {
                    updateProfileData(it)
                    progressContainer.visibility = GONE
                }
            }
        }, onFailure = {
            progressContainer.visibility = GONE
            Toast.makeText(activity, "Failed to retrieve profile data", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        progressContainer = view.progressBarContainer
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun updateProfileData(profile: Profile) {
        val dateFormat = SimpleDateFormat("dd-mm-yyyy", Locale.getDefault())
        val date = dateFormat.parse(profile.birthDate)
        userName.text = profile.username
        firstName.text = profile.firstName
        lastName.text = profile.lastName
        email.text = profile.email
        phone.text = profile.phoneNumber
        birthDate.text = dateFormat.format(date)
        pesel.text = profile.pesel
    }

    interface OnFragmentInteractionListener {
        fun onProfileInteraction(uri: Uri)
    }
}
