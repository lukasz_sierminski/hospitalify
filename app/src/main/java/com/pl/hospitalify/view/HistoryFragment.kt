package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.view.adapter.ExistingVisitAdapter
import com.pl.hospitalify.view.adapter.PrescriptionAdapter
import com.pl.hospitalify.view.holder.ExistingVisitViewHolder
import com.pl.hospitalify.viewmodel.ExistingVisitViewModel
import com.pl.hospitalify.viewmodel.PrescriptionViewModel
import kotlinx.android.synthetic.main.history.view.*

class HistoryFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var prescriptionsRecyclerView: RecyclerView
    private lateinit var visitsRecyclerView: RecyclerView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.history, container, false)
        prescriptionsRecyclerView = view.prescriptionsRecyclerView
        visitsRecyclerView = view.visitsRecyclerView
        setupRecyclerViews()
        initializeAdapters()
        return view
    }

    private fun setupRecyclerViews() {
        val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        prescriptionsRecyclerView.layoutManager = LinearLayoutManager(activity)
        prescriptionsRecyclerView.addItemDecoration(itemDecoration)

        visitsRecyclerView.layoutManager = LinearLayoutManager(activity)
        visitsRecyclerView.addItemDecoration(itemDecoration)
    }

    private fun initializeAdapters() {
        val prescriptionsAdapter = PrescriptionAdapter()
        val prescriptionViewModel = PrescriptionViewModel()
        prescriptionViewModel.getPrescriptions().observe(this, Observer {
            prescriptionsAdapter.prescriptions = it
            prescriptionsAdapter.notifyDataSetChanged()
        })

        val visitsAdapter = ExistingVisitAdapter { visit ->
            listener?.onVisitCanceled(visit.id)
        }
        val visitViewModel = ExistingVisitViewModel()
        visitViewModel.getVisits().observe(this, Observer {
            visitsAdapter.visits = it
            visitsAdapter.notifyDataSetChanged()
        })

        prescriptionsRecyclerView.adapter = prescriptionsAdapter
        visitsRecyclerView.adapter = visitsAdapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onVisitCanceled(visitId: Int)
    }
}
