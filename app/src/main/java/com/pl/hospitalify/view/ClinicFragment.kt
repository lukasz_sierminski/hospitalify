package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.model.Clinic
import com.pl.hospitalify.view.adapter.ClinicAdapter
import com.pl.hospitalify.viewmodel.ClinicViewModel
import kotlinx.android.synthetic.main.fragment_clinic.view.*

class ClinicFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_clinic, container, false)
        recyclerView = view.clinicsRecyclerView
        setupRecyclerView(recyclerView)
        initializeClinicAdapter(recyclerView)
        return view
    }

    private fun initializeClinicAdapter(recyclerView: RecyclerView) {
        val clinicAdapter = ClinicAdapter { listener?.onClinicClick(it) }
        clinicAdapter.clinics = emptyList()
        val clinicViewModel = ClinicViewModel()
        clinicViewModel.getClinics().observe(this, Observer {
            clinicAdapter.clinics = it
            clinicAdapter.notifyDataSetChanged()
        })
        recyclerView.adapter = clinicAdapter
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(itemDecoration)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onClinicClick(clinic: Clinic)
    }
}
