package com.pl.hospitalify.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.pl.hospitalify.R
import com.pl.hospitalify.data.LoginInfo
import com.pl.hospitalify.data.Registration
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.token.SharedPreferencesTokenStorage
import com.pl.hospitalify.token.TokenStorage

class StartingActivity : FragmentActivity(),
    StartingFragment.OnFragmentInteractionListener,
    LoginFragment.OnFragmentInteractionListener,
    RegistrationFragment.OnFragmentInteractionListener {

    private lateinit var navController: NavController
    private lateinit var tokenStorage: TokenStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starting)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        tokenStorage = SharedPreferencesTokenStorage(applicationContext)
        skipLogging()
    }

    override fun onLogin(loginInfo: LoginInfo) {
        ServiceProvider.healthcareRepository.logIn(loginInfo, onSuccess = { response ->
            if (response.isSuccessful) {
                Toast.makeText(this, "Login Success!", Toast.LENGTH_SHORT).show()
                response.body()?.token?.let { token ->
                    onAuthTokenReceived(token)
                }
            } else {
                Toast.makeText(this, "Login failed with code ${response.code()}", Toast.LENGTH_SHORT).show()
            }
        }, onFailure = { throwable ->
            Toast.makeText(this, "Login failed: ${throwable.message}", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onRegister(registration: Registration) {
        ServiceProvider.healthcareRepository.registerUser(registration, onSuccess = {
            Toast.makeText(this, "Registration Success!", Toast.LENGTH_SHORT).show()
        }, onFailure = { throwable ->
            Toast.makeText(this, "Registration failed: ${throwable.message}", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onLoginButtonHit() {
        navController.navigate(R.id.loginFragment)
    }

    override fun onRegisterButtonHit() {
        navController.navigate(R.id.registrationFragment)
    }

    private fun skipLogging() {
        if (isLoggedIn()) {
            navController.navigate(R.id.mainActivity)
        }
    }

    private fun onAuthTokenReceived(token: String) {
        tokenStorage.store(token)
        ServiceProvider.initialize(applicationContext)
        val activityNavigator = ActivityNavigator(applicationContext)
        val mainActivityDestination = activityNavigator.createDestination()
        mainActivityDestination.intent = Intent(this, MainActivity::class.java)
        activityNavigator.navigate(mainActivityDestination, null, null, null)
    }

    private fun isLoggedIn(): Boolean = tokenStorage.get().isNotEmpty()
}