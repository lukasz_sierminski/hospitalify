package com.pl.hospitalify.view.holder

import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.databinding.ClinicItemRowBinding
import com.pl.hospitalify.model.Clinic

class ClinicViewHolder constructor(private val binding: ClinicItemRowBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(clinic: Clinic) {
        binding.clinic = clinic
        binding.executePendingBindings()
    }
}