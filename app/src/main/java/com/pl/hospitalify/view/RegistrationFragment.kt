package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pl.hospitalify.R
import com.pl.hospitalify.data.Registration
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.fragment_registration.view.*

class RegistrationFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_registration, container, false)
        setupRegistrationButton(view)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setupRegistrationButton(rootView: View) {
        rootView.registerButton.setOnClickListener {
            listener?.onRegister(buildRegistrationData())
        }
    }

    private fun buildRegistrationData(): Registration {
        return Registration(
            userNameEdit.text.toString(),
            passwordEdit.text.toString(),
            firstNameEdit.text.toString(),
            lastNameEdit.text.toString(),
            emailEdit.text.toString(),
            peselEdit.text.toString(),
            phoneEdit.text.toString(),
            birthDateEdit.text.toString())
    }

    interface OnFragmentInteractionListener {
        fun onRegister(registration: Registration)
    }
}
