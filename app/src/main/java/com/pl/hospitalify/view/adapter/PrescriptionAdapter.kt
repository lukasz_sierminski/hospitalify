package com.pl.hospitalify.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.model.Prescription
import com.pl.hospitalify.view.holder.PrescriptionViewHolder

class PrescriptionAdapter : RecyclerView.Adapter<PrescriptionViewHolder>() {
    var prescriptions: List<Prescription> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrescriptionViewHolder {
        return PrescriptionViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.prescription_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = prescriptions.size

    override fun onBindViewHolder(holder: PrescriptionViewHolder, position: Int) {
        val clinic = prescriptions[position]
        holder.bind(clinic)
    }
}