package com.pl.hospitalify.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.model.ExistingVisit
import com.pl.hospitalify.view.holder.ExistingVisitViewHolder

class ExistingVisitAdapter(private val onItemClicked: (visit: ExistingVisit) -> Unit) : RecyclerView.Adapter<ExistingVisitViewHolder>() {
    var visits: List<ExistingVisit> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExistingVisitViewHolder {
        return ExistingVisitViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.visit_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = visits.size

    override fun onBindViewHolder(holder: ExistingVisitViewHolder, position: Int) {
        val visit = visits[position]
        if (visit.visitStatus == "AWAITING") {
            val button = holder.itemView.findViewById<Button>(R.id.cancelVisitButton)
            button.visibility = View.VISIBLE
            button.setOnClickListener{
                onItemClicked(visit)
            }
        }
        holder.bind(visit)
    }
}