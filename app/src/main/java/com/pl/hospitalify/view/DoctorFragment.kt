package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.Logger
import com.pl.hospitalify.R
import com.pl.hospitalify.model.Doctor
import com.pl.hospitalify.view.adapter.DoctorAdapter
import com.pl.hospitalify.viewmodel.DoctorViewModel
import kotlinx.android.synthetic.main.fragment_doctor.view.*
import java.util.*

internal const val ARG_UNIT_ID = "unit_id"
internal const val ARG_UNIT_NAME = "unit_name"

class DoctorFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var spinner: AppCompatSpinner
    private lateinit var datePicker: DatePicker
    private lateinit var symptomsEdit: EditText
    private lateinit var unitName: String
    private lateinit var adapter: DoctorAdapter
    private lateinit var specializations: List<String>
    private var unitId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            unitId = it.getInt(ARG_UNIT_ID)
            unitName = it.getString(ARG_UNIT_NAME, "clinic")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_doctor, container, false)
        view.doctorsAvailableLabel.text = getString(R.string.doctors_available, unitName)
        spinner = view.specialisationSpinner
        recyclerView = view.doctorsRecyclerView
        datePicker = view.visitDatePicker
        symptomsEdit = view.symptomsEdit
        setupDatePicker()
        setupRecyclerView()
        setupSpecialisationSpinner()
        initializeDoctorAdapter()
        loadDoctors()
        return view
    }

    private fun initializeDoctorAdapter() {
        adapter = DoctorAdapter { doctor ->
            val calendar = Calendar.getInstance()
            calendar.set(datePicker.year, datePicker.month, datePicker.dayOfMonth)
            if (calendar.time.before(Date())) {
                Toast.makeText(activity, "Pick correct date!", Toast.LENGTH_SHORT).show()
            } else {
                listener?.onDoctorClicked(visitDateFromCalendar(calendar), doctor, symptomsEdit.text.toString())
            }
        }
        adapter.doctors = emptyList()
        val doctorViewModel = DoctorViewModel()
        doctorViewModel.getDoctors(unitId).observe(this, Observer {
            adapter.doctors = it
            adapter.notifyDataSetChanged()
        })
        recyclerView.adapter = adapter
    }

    private fun setupDatePicker() {
        val calendar = Calendar.getInstance()
        datePicker.init(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ) { _, year, month, day ->
            onVisitDatePicked("$year-$month-$day")
        }
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val itemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(itemDecoration)
    }

    private fun setupSpecialisationSpinner() {
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                adapter.specializationFilter = ""
                adapter.notifyDataSetChanged()
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                adapter.specializationFilter = specializations[p2]
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun loadDoctors() {
        val doctorViewModel = DoctorViewModel()
        doctorViewModel.getDoctors(unitId).observe(this, Observer {
            onDoctorsLoaded(it)
        })
    }

    private fun onDoctorsLoaded(doctors: List<Doctor>) {
        Log.e("TEST", "DOCTORS $doctors")
        specializations = doctors.map { doctor -> doctor.specialisation }
            .distinct()
            .toList()
        spinner.adapter = ArrayAdapter<String>(
            activity!!.applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            specializations
        )
        adapter.notifyDataSetChanged()
    }

    private fun visitDateFromCalendar(calendar: Calendar): String {
        val monthInt = calendar.get(Calendar.MONTH) + 1
        var month = monthInt.toString()
        if (month.length == 1) {
            month = "0$month"
        }
        return "${calendar.get(Calendar.YEAR)}-$month-${calendar.get(Calendar.DAY_OF_MONTH)}"
    }

    private fun onVisitDatePicked(date: String) {
        Logger.debug("Visit date picked: $date")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onDoctorClicked(date: String, doctor: Doctor, symptoms: String)
    }
}
