package com.pl.hospitalify.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.model.Doctor
import com.pl.hospitalify.view.holder.DoctorViewHolder

class DoctorAdapter(private val onItemClicked: (doctor: Doctor) -> Unit) : RecyclerView.Adapter<DoctorViewHolder>(){

    lateinit var doctors: List<Doctor>
    var specializationFilter = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorViewHolder {
        return DoctorViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.doctor_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return if (specializationFilter.isNotEmpty()) {
            doctors
                .filter { it.specialisation == specializationFilter }
                .size
        } else {
            doctors.size
        }
    }

    override fun onBindViewHolder(holder: DoctorViewHolder, position: Int) {
        val doctor: Doctor
        if (specializationFilter.isNotEmpty()) {
            doctor = doctors.filter { it.specialisation == specializationFilter }[position]
        } else {
            doctor = doctors[position]
        }
        holder.itemView.setOnClickListener{
            onItemClicked(doctor)
        }
        holder.bind(doctor)
    }
}