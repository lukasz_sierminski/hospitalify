package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pl.hospitalify.R
import kotlinx.android.synthetic.main.fragment_starting.view.*

class StartingFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_starting, container, false)
        setupButtons(view)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setupButtons(rootView: View) {
        rootView.loginButton.setOnClickListener {
            listener?.onLoginButtonHit()
        }
        rootView.registrationButton.setOnClickListener {
            listener?.onRegisterButtonHit()
        }
    }

    interface OnFragmentInteractionListener {
        fun onLoginButtonHit()
        fun onRegisterButtonHit()
    }
}
