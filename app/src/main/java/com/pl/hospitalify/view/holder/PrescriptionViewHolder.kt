package com.pl.hospitalify.view.holder

import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.databinding.PrescriptionItemRowBinding
import com.pl.hospitalify.model.Prescription

class PrescriptionViewHolder constructor(private val binding: PrescriptionItemRowBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(prescription: Prescription) {
        binding.prescription = prescription
        binding.executePendingBindings()
    }
}