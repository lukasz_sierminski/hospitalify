package com.pl.hospitalify.view.holder

import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.databinding.VisitItemRowBinding
import com.pl.hospitalify.model.ExistingVisit

class ExistingVisitViewHolder constructor(private val binding: VisitItemRowBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(visit: ExistingVisit) {
        binding.visit = visit
        binding.executePendingBindings()
    }
}