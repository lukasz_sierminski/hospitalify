package com.pl.hospitalify.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pl.hospitalify.R
import com.pl.hospitalify.data.LoginInfo
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_starting.view.*

class LoginFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        setupLoginButton(view)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setupLoginButton(rootView: View) {
        rootView.loginButton.setOnClickListener {
            listener?.onLogin(buildLoginInfo())
        }
    }

    private fun buildLoginInfo() = LoginInfo(userNameEdit.text.toString(), passwordEdit.text.toString())

    interface OnFragmentInteractionListener {
        fun onLogin(loginInfo: LoginInfo)
    }
}
