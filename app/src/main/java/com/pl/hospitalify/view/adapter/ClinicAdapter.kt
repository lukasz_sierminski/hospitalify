package com.pl.hospitalify.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.R
import com.pl.hospitalify.model.Clinic
import com.pl.hospitalify.view.holder.ClinicViewHolder

class ClinicAdapter(private val onItemClicked: (clinic: Clinic) -> Unit) : RecyclerView.Adapter<ClinicViewHolder>() {

    lateinit var clinics: List<Clinic>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClinicViewHolder {
        return ClinicViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.clinic_item_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = clinics.size

    override fun onBindViewHolder(holder: ClinicViewHolder, position: Int) {
        val clinic = clinics[position]
        holder.itemView.setOnClickListener{
            onItemClicked(clinic)
        }
        holder.bind(clinic)
    }
}