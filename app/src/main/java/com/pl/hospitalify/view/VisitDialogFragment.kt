package com.pl.hospitalify.view


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.fragment.app.DialogFragment
import com.pl.hospitalify.Logger
import com.pl.hospitalify.R
import kotlinx.android.synthetic.main.fragment_visit_dialog.view.*

internal const val ARG_HOURS = "hours"

class VisitDialogFragment : DialogFragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var hours: List<String>
    private lateinit var radioGroup: RadioGroup
    private val ids = mutableListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val array = it.getStringArray(ARG_HOURS)
            array?.let {
                hours = array.toList()

            }
            Logger.debug("Printing working hours: $hours")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_visit_dialog, container, false)
        radioGroup = view.timeRadioGroup
        setupRadioButtons(view.timeRadioGroup)
        setupAddVisitButton(view.addVisitButton)
        return view
    }

    private fun setupRadioButtons(radioGroup: RadioGroup) {
        hours.forEach{
            val radioButton = RadioButton(activity)
            val id = View.generateViewId()
            ids.add(id)
            radioButton.id = id
            radioButton.text = it
            radioGroup.addView(radioButton)
        }
        radioGroup.setOnCheckedChangeListener { _, buttonId ->
            val index = ids.indexOf(buttonId)
            Logger.debug("On radio button checked: ${hours[index]}")
        }
    }

    private fun setupAddVisitButton(button: Button) {
        button.setOnClickListener {
            val index = ids.indexOf(radioGroup.checkedRadioButtonId)
            listener?.onAddVisitClick(hours[index])
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onAddVisitClick(hour: String)
    }
}
