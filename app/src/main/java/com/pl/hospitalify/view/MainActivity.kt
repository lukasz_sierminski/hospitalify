package com.pl.hospitalify.view

import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.pl.hospitalify.R
import com.pl.hospitalify.data.ServiceProvider
import com.pl.hospitalify.model.Clinic
import com.pl.hospitalify.model.Doctor
import com.pl.hospitalify.model.Visit
import com.pl.hospitalify.model.VisitCancel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : FragmentActivity(),
    HistoryFragment.OnFragmentInteractionListener,
    ProfileFragment.OnFragmentInteractionListener,
    ClinicFragment.OnFragmentInteractionListener,
    DoctorFragment.OnFragmentInteractionListener,
    VisitDialogFragment.OnFragmentInteractionListener {
    private lateinit var navController: NavController
    private var clinicId = 0
    private var doctorId = 0
    private lateinit var visitDate: String
    private lateinit var symptoms: String

    override fun onVisitCanceled(visitId: Int) {
        ServiceProvider.healthcareRepository.cancelVisit(VisitCancel(visitId),
            onSuccess = { response ->
                if (response.isSuccessful) {
                    Toast.makeText(this, "Successfully canceled visit!", Toast.LENGTH_SHORT).show()
                }
            }, onFailure = {
                Toast.makeText(this, "Failed to cancel visit", Toast.LENGTH_SHORT).show()
            })
    }

    override fun onAddVisitClick(hour: String) {
        val visit = Visit(doctorId, clinicId, "${visitDate}T$hour", symptoms)
        ServiceProvider.healthcareRepository.addVisit(visit,
            onSuccess = { response ->
                if (response.isSuccessful) {
                    navController.popBackStack()
                    Toast.makeText(this, "Successfully scheduled visit!", Toast.LENGTH_SHORT).show()
                }
            }, onFailure = {
                Toast.makeText(this, "Failed to schedule visit", Toast.LENGTH_SHORT).show()
            })
    }

    override fun onDoctorClicked(date: String, doctor: Doctor, symptoms: String) {
        visitDate = date
        doctorId = doctor.id
        this.symptoms = symptoms
        ServiceProvider.healthcareRepository.getDoctorWorkingHours(doctor.id, date,
            onSuccess = { response ->
                if (response.isSuccessful) {
                    val args = Bundle()
                    response.body()?.let { args.putStringArray(ARG_HOURS, it.toTypedArray()) }
                    navController.navigate(R.id.visitDialogFragment, args)
                }
            }, onFailure = {
                Toast.makeText(this, "Failed to fetch doctor working hours", Toast.LENGTH_SHORT).show()
            })
    }

    override fun onProfileInteraction(uri: Uri) {
    }

    override fun onClinicClick(clinic: Clinic) {
        clinicId = clinic.id
        val args = Bundle()
        args.putInt(ARG_UNIT_ID, clinic.id)
        args.putString(ARG_UNIT_NAME, clinic.unitName)
        navController.navigate(R.id.doctorFragment, args)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navHost = nav_host_fragment as NavHostFragment
        NavigationUI.setupWithNavController(bottom_navigation, navHost.navController)
        navController = navHost.navController
    }
}
