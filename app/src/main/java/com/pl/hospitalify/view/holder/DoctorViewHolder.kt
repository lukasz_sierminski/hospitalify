package com.pl.hospitalify.view.holder

import androidx.recyclerview.widget.RecyclerView
import com.pl.hospitalify.model.Doctor

class DoctorViewHolder constructor(private val binding: com.pl.hospitalify.databinding.DoctorItemRowBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(doctor: Doctor) {
        binding.doctor = doctor
        binding.executePendingBindings()
    }
}