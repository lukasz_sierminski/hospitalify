package com.pl.hospitalify.token

interface TokenStorage {
    fun store(token: String)
    fun get(): String
}