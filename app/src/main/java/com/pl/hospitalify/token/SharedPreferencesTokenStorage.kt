package com.pl.hospitalify.token

import android.content.Context
import android.content.SharedPreferences
import com.pl.hospitalify.Logger

class SharedPreferencesTokenStorage(context: Context) : TokenStorage {

    private val preferences: SharedPreferences

    companion object {
        private const val PREFERENCES_NAME = "HOSPITALIFY_STORAGE"
        private const val TOKEN_KEY = "token"
    }

    init {
        preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
    }
    override fun store(token: String) {
        Logger.debug("Storing token: $token")
        preferences.edit().putString(TOKEN_KEY, token).apply()
    }

    override fun get(): String {
        val token = preferences.getString(TOKEN_KEY, "") ?: ""
        Logger.debug("Retrieved token: $token")
        return token
    }
}