package com.pl.hospitalify.model

data class Address(val street: String,
                   val streetNumber: String,
                   val zipCode: String,
                   val city: String,
                   val country: String)