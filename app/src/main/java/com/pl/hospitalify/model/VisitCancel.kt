package com.pl.hospitalify.model

data class VisitCancel(val visitId: Int)