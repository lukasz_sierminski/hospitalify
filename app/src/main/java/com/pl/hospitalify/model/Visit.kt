package com.pl.hospitalify.model

data class Visit(val doctorId: Int,
                 val healthcareUnitId: Int,
                 val visitDate: String,
                 val symptoms: String)