package com.pl.hospitalify.model

data class Prescription(val id: Int,
                        val dateOfIssue: String,
                        val expirationDate: String,
                        val doctor: Doctor,
                        val content: String)