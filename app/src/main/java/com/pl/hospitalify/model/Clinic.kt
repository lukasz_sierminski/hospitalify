package com.pl.hospitalify.model

data class Clinic(val id: Int,
                  val unitName: String,
                  val address: String,
                  val postalCode: String,
                  val city: String)