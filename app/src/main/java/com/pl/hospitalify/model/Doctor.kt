package com.pl.hospitalify.model

data class Doctor(val id: Int,
                  val firstName: String,
                  val lastName: String,
                  val email: String,
                  val phoneNumber: String,
                  val specialisation: String)