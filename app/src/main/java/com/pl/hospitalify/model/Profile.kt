package com.pl.hospitalify.model

data class Profile(val id: Int,
                   val patientId: Int,
                   val username: String,
                   val firstName: String,
                   val lastName: String,
                   val email: String,
                   val birthDate: String,
                   val phoneNumber: String,
                   val pesel: String)