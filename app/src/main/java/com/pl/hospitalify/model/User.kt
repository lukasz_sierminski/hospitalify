package com.pl.hospitalify.model

import java.util.*

data class User(val id: Int,
                val name: String,
                val surname: String,
                val birthDate: Date,
                val pesel: String)