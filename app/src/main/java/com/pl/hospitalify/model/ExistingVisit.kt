package com.pl.hospitalify.model

data class ExistingVisit(val id: Int,
                         val healthcareUnitId: Int,
                         val visitDate: String,
                         val symptoms: String,
                         val doctor: Doctor,
                         val healthcareUnit: Clinic,
                         val visitStatus: String)