package com.pl.hospitalify

import android.util.Log

class Logger {
    companion object {
        private const val TAG = "Hospitalify"

        fun debug(tag: String = TAG, message: String) = Log.d(tag, message)

        fun debug(message: String) = debug(TAG, message = message)

        fun info(tag: String = TAG, message: String) = Log.i(tag, message)

        fun error(tag: String = TAG, message: String) = Log.e(tag, message)
    }
}