package com.pl.hospitalify.data

import android.content.Context
import com.google.gson.GsonBuilder
import com.pl.hospitalify.token.SharedPreferencesTokenStorage
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient

private const val DEFAULT_URL = "http://localhost:45555/"

class ServiceProvider private constructor(context: Context) {

    private val healthcareRepository: HealthcareRepository

    init {
        healthcareRepository = HealthcareRepository(createHealthcareService(context))
    }

    companion object {
        private lateinit var serviceProvider: ServiceProvider
        val healthcareRepository by lazy { serviceProvider.healthcareRepository }

        fun initialize(context: Context) {
            serviceProvider = ServiceProvider(context)
        }

        private fun createHealthcareService(context: Context): HealthcareService {
            val clientBuilder = OkHttpClient.Builder()
            setupLoggingInterceptor(clientBuilder)
            setupAuthorizationInterceptor(context, clientBuilder)
            return Retrofit.Builder()
                .baseUrl(DEFAULT_URL)
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                .client(clientBuilder.build())
                .build()
                .create(HealthcareService::class.java)
        }

        private fun setupLoggingInterceptor(builder: OkHttpClient.Builder) {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        private fun setupAuthorizationInterceptor(context: Context, builder: OkHttpClient.Builder) {
            val token = SharedPreferencesTokenStorage(context.applicationContext).get()
            if (token.isNotEmpty()) {
                builder.addInterceptor { chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer $token")
                        .build()
                    chain.proceed(request)
                }
            }
        }

        private fun createGson() = GsonBuilder()
            .setPrettyPrinting()
            .setLenient()
            .create()
    }
}