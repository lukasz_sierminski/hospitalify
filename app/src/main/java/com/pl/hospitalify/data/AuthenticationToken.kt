package com.pl.hospitalify.data

data class AuthenticationToken(val token: String)