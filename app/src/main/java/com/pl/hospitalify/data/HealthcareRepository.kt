package com.pl.hospitalify.data

import com.pl.hospitalify.Logger
import com.pl.hospitalify.model.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HealthcareRepository(private val service: HealthcareService) {

    fun registerUser(registration: Registration,
                     onSuccess: (Response<ResponseBody>) -> Unit,
                     onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending registration request")
        service.register(registration).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Logger.debug("Failed to send registration request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Logger.debug("Successfuly sent registration request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun logIn(loginInfo: LoginInfo,
              onSuccess: (Response<AuthenticationToken>) -> Unit,
              onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending login request")
        service.logIn(loginInfo).enqueue(object : Callback<AuthenticationToken> {
            override fun onFailure(call: Call<AuthenticationToken>, t: Throwable) {
                Logger.debug("Failed to send login request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<AuthenticationToken>, response: Response<AuthenticationToken>) {
                Logger.debug("Successfuly sent login request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getProfile(onSuccess: (Response<Profile>) -> Unit,
                   onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get profile request")
        service.getProfile().enqueue(object : Callback<Profile> {
            override fun onFailure(call: Call<Profile>, t: Throwable) {
                Logger.debug("Failed to send get profile request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                Logger.debug("Successfuly sent get profile request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getHealthCareUnits(onSuccess: (Response<List<Clinic>>) -> Unit,
                           onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get units request")
        service.getUnits().enqueue(object : Callback<List<Clinic>> {
            override fun onFailure(call: Call<List<Clinic>>, t: Throwable) {
                Logger.debug("Failed to send get units request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<List<Clinic>>, response: Response<List<Clinic>>) {
                Logger.debug("Successfuly sent get units request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getDoctors(unitId: Int,
                   onSuccess: (Response<List<Doctor>>) -> Unit,
                   onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get doctors request")
        service.getDoctors(unitId).enqueue(object : Callback<List<Doctor>> {
            override fun onFailure(call: Call<List<Doctor>>, t: Throwable) {
                Logger.debug("Failed to send get doctors request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<List<Doctor>>, response: Response<List<Doctor>>) {
                Logger.debug("Successfuly sent get doctors request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getDoctorWorkingHours(doctorId: Int,
                              date: String,
                              onSuccess: (Response<List<String>>) -> Unit,
                              onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get doctors working hours request")
        service.getDoctorWorkingHours(doctorId, date).enqueue(object : Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                Logger.debug("Failed to send get doctors working hours request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {
                Logger.debug("Successfuly sent get doctors request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun addVisit(visit: Visit, onSuccess: (Response<ExistingVisit>) -> Unit, onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending add visit request")
        service.addVisit(visit).enqueue(object : Callback<ExistingVisit> {
            override fun onFailure(call: Call<ExistingVisit>, t: Throwable) {
                Logger.debug("Failed to send add visit request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<ExistingVisit>, response: Response<ExistingVisit>) {
                Logger.debug("Successfuly sent add visit request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun cancelVisit(visitCancel: VisitCancel,
                    onSuccess: (Response<ResponseBody>) -> Unit,
                    onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending cancel visit request")
        service.cancelVisit(visitCancel).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Logger.debug("Failed to send cancel visit request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Logger.debug("Successfuly sent cancel visit request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getVisitsHistory(onSuccess: (Response<List<ExistingVisit>>) -> Unit,
                         onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get visits history request")
        service.getVisitsHistory().enqueue(object : Callback<List<ExistingVisit>> {
            override fun onFailure(call: Call<List<ExistingVisit>>, t: Throwable) {
                Logger.debug("Failed to send get visits history request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<List<ExistingVisit>>, response: Response<List<ExistingVisit>>) {
                Logger.debug("Successfuly sent get visits history request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }

    fun getPrescriptions(onSuccess: (Response<List<Prescription>>) -> Unit,
                         onFailure: (throwable: Throwable) -> Unit) {
        Logger.debug("Sending get visits request")
        service.getPrescriptions().enqueue(object : Callback<List<Prescription>> {
            override fun onFailure(call: Call<List<Prescription>>, t: Throwable) {
                Logger.debug("Failed to send get visits request: ${t.message}")
                onFailure(t)
            }

            override fun onResponse(call: Call<List<Prescription>>, response: Response<List<Prescription>>) {
                Logger.debug("Successfuly sent get visits request, response code ${response.code()} ${response.body()}")
                onSuccess(response)
            }
        })
    }
}