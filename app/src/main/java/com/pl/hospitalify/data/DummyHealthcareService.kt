package com.pl.hospitalify.data

import com.pl.hospitalify.model.*
import okhttp3.ResponseBody
import retrofit2.Call

class DummyHealthcareService : HealthcareService {
    override fun register(registration: Registration): Call<ResponseBody> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logIn(loginInfo: LoginInfo): Call<AuthenticationToken> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProfile(): Call<Profile> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUnits(): Call<List<Clinic>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDoctors(unit: Int): Call<List<Doctor>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDoctorWorkingHours(doctorId: Int, date: String): Call<List<String>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addVisit(visit: Visit): Call<ExistingVisit> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun cancelVisit(visitCancel: VisitCancel): Call<ResponseBody> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getVisitsHistory(): Call<List<ExistingVisit>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPrescriptions(): Call<List<Prescription>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}