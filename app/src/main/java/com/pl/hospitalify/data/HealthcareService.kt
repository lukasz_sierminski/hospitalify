package com.pl.hospitalify.data

import com.pl.hospitalify.model.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface HealthcareService {

    @Headers("Content-Type: application/json")
    @POST("/patient/register")
    fun register(@Body registration: Registration): Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("/auth")
    fun logIn(@Body loginInfo: LoginInfo): Call<AuthenticationToken>

    @GET("/me")
    fun getProfile(): Call<Profile>

    @GET("/unit")
    fun getUnits(): Call<List<Clinic>>

    @GET("/doctor")
    fun getDoctors(@Query("unit") unit: Int): Call<List<Doctor>>

    @GET("/doctor/{id}/free")
    fun getDoctorWorkingHours(@Path("id") doctorId: Int, @Query("visitDate") date: String): Call<List<String>>

    @Headers("Content-Type: application/json")
    @POST("/visit/add")
    fun addVisit(@Body visit: Visit): Call<ExistingVisit>

    @Headers("Content-Type: application/json")
    @POST("/visit/cancel")
    fun cancelVisit(@Body visitCancel: VisitCancel): Call<ResponseBody>

    @GET("/visit")
    fun getVisitsHistory(): Call<List<ExistingVisit>>

    @GET("/prescription")
    fun getPrescriptions(): Call<List<Prescription>>
}