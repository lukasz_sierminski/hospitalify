package com.pl.hospitalify.data

data class Registration(val username: String,
                        val password: String,
                        val firstName: String,
                        val lastName: String,
                        val email: String,
                        val pesel: String,
                        val phoneNumber: String,
                        val birthDate: String)
/*
{
"username":"test_user",
"password":"test123",
"firstName":"testFirstName",
"lastName":"testLastName",
"email":"testemail@test.com",
"pesel":"95112800000",
"phoneNumber":"000000000",
"birthDate":"1995-11-28"
}
        */