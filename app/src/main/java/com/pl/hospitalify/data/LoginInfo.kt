package com.pl.hospitalify.data

data class LoginInfo(val username: String,
                     val password: String)
